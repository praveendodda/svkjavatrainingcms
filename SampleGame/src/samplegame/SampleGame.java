package samplegame;

import samplegame.cli.CliRunner;

/**
 * 
 * Sample Game Application
 * 
 * Objects Created:
 * 
 * samplegame.cli:
 * 
 * 
 * samplegame.engine:
 * Engine
 * Player
 * 
 * samplegame.engine.board:
 * Board
 * Cell
 * 
 * samplegame.engine.enemy
 * Enemy
 * 
 * 
 * ------------------------------
 * |   |   |   |    |
 * |   | P |   |  E |
 * |   |   |   |    |
 * |   |   |   |    |
 * |   |   |   | 
 * |   |   | E |
 * |   |   |   |
 * --------------------------------
 *
 * @author pvik
 */
public class SampleGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        CliRunner r = new CliRunner();
        
        r.run();
    }

}
