package samplegame.engine;

/**
 *
 * @author pvik
 */
public class Player {
    String name;
    
    int scrore;
    
    int health;
    
    int noOfTimesPlayed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScrore() {
        return scrore;
    }

    public void setScrore(int scrore) {
        this.scrore = scrore;
    }

    public int getHp() {
        return health;
    }

    public void setHp(int hp) {
        this.health = hp;
    }

    public int getNoOfTimesPlayed() {
        return noOfTimesPlayed;
    }

    public void setNoOfTimesPlayed(int noOfTimesPlayed) {
        this.noOfTimesPlayed = noOfTimesPlayed;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", scrore=" + scrore + ", hp=" + health + ", noOfTimesPlayed=" + noOfTimesPlayed + '}';
    }
    
    
}
