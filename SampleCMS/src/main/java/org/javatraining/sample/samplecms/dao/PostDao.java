/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javatraining.sample.samplecms.dao;

import org.javatraining.sample.samplecms.model.Post;

/**
 *
 * @author team
 */
public abstract class PostDao {
     
    public abstract void submitPost(Post p);
    
    public abstract Post retrievePost(int postid);
    
    public abstract void deletePost(Post p);
    
}
