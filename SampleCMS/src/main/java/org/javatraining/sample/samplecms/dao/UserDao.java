package org.javatraining.sample.samplecms.dao;

import java.util.List;
import org.javatraining.sample.samplecms.model.User;

/**
 *
 * @author pvik
 */
public abstract class UserDao {
    
    public abstract void insertUser(User u) throws DaoException;
    
    public abstract User getUser(String userid);
    
    public abstract List<User> getUsers(String userName);
    
}
